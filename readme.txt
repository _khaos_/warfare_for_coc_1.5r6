The repo has been updated for Call of Chernobyl 1.5.

New version of warfare, now with less overwriting! The goal of this version of warfare is to be less dependent on hacking together sim_squad_scripted.script and smart_terrain.script. 
Instead, callbacks will be used which will make it much easier to read the code and write more advanced faction AI.

To install, place the gamedata directory and fsgame.ltx into your Call of Chernobyl directory, ovewriting both which are there already.

I'll be making the readme more robust in the future.